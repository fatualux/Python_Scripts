# Python Scripts

### These are scripts in Python written for various purposes.

## INSTALLATION

```
Simply clone this repository.
```

## DEPENDENCIES

- python
- virtualenv


## LICENSE

[![License](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

This project is licensed under the GPLv3 license.
See LICENSE file for more details.
